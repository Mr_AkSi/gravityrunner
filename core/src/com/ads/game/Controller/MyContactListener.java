package com.ads.game.Controller;

import com.ads.game.GravityRunner;
import com.ads.game.Screens.GameScreen;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;

import java.io.FileNotFoundException;

/**
 * Created by Aksi on 11.04.2016.
 */
    public class MyContactListener implements ContactListener {
    Fixture fa;
    Fixture fb;

    public MyContactListener(World physicsWorld) {

    }

    @Override

    public void endContact(Contact contact) {
        fa = contact.getFixtureA();
        fb = contact.getFixtureB();

        if (fa == null || fb == null) return;
        if (fa.getUserData() == null || fb.getUserData() == null) return;
        if ((fa.getUserData().equals("switch") && fb.getUserData().equals("playerHand"))||(fb.getUserData().equals("switch") && fa.getUserData().equals("playerHand")))
            GravityRunner.nearSwitch = State.NO;
    }

    @Override

    public void beginContact(Contact contact) {
        fa = contact.getFixtureA();
        fb = contact.getFixtureB();

        if (fa == null || fb == null) return;
        if (fa.getUserData() == null || fb.getUserData() == null) return;
        if (fa.getUserData().equals("playerFeet") && fb.getUserData().equals("wall"))
            GravityRunner.inAir = State.NO;
        if (fa.getUserData().equals("playerFeet") && fb.getUserData().equals("jumpplatform"))
            GravityRunner.jumpboost = State.YES;
        if (fa.getUserData().equals("playerBody") && fb.getUserData().equals("dorexit")){
            GravityRunner.getInstance().showMenu();
            switch (GravityRunner.getInstance().gameScreen.levelNum){
                case 1: GravityRunner.Level1Completed = true; break;
                case 2: GravityRunner.Level2Completed = true; break;
                case 3: GravityRunner.Level3Completed = true; break;
                case 4: GravityRunner.Level4Completed = true; break;
                case 5: GravityRunner.Level5Completed = true; break;
                case 6: GravityRunner.Level6Completed = true; break;
                case 7: GravityRunner.Level7Completed = true; break;
                case 8: GravityRunner.Level8Completed = true; break;
            }
            GravityRunner.getInstance().gameScreen.dispose();
            GravityRunner.getInstance().statsScreen.setStats();
            GravityRunner.timerLVL1 = 0;
            GravityRunner.timerLVL2 = 0;
            GravityRunner.timerLVL3 = 0;
            GravityRunner.timerLVL4 = 0;
            GravityRunner.timerLVL5 = 0;
            GravityRunner.timerLVL6 = 0;
            GravityRunner.timerLVL7 = 0;
            GravityRunner.timerLVL8 = 0;
        }
        if ((fa.getUserData().equals("switch") && fb.getUserData().equals("playerHand"))||(fb.getUserData().equals("switch") && fa.getUserData().equals("playerHand"))) {
            GravityRunner.nearSwitch = State.YES;
        }
        System.out.println("Collision. A = " + fa.getUserData() + " B = " + fb.getUserData() + " Jumped = " + GravityRunner.inAir + " Near switch = " + GravityRunner.nearSwitch);
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}