package com.ads.game.Controller;

import com.ads.game.GravityRunner;
import com.ads.game.Model.HUD;
import com.ads.game.Model.Level;
import com.ads.game.Model.Player;
import com.ads.game.View.FontActor;
import com.ads.game.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;

import static com.ads.game.GravityRunner.*;
import static com.badlogic.gdx.Input.Keys.A;
import static com.badlogic.gdx.Input.Keys.D;
import static com.badlogic.gdx.Input.Keys.DOWN;
import static com.badlogic.gdx.Input.Keys.E;
import static com.badlogic.gdx.Input.Keys.ENTER;
import static com.badlogic.gdx.Input.Keys.ESCAPE;
import static com.badlogic.gdx.Input.Keys.LEFT;
import static com.badlogic.gdx.Input.Keys.NUM_1;
import static com.badlogic.gdx.Input.Keys.NUM_2;
import static com.badlogic.gdx.Input.Keys.NUM_3;
import static com.badlogic.gdx.Input.Keys.R;
import static com.badlogic.gdx.Input.Keys.RIGHT;
import static com.badlogic.gdx.Input.Keys.S;
import static com.badlogic.gdx.Input.Keys.SPACE;
import static com.badlogic.gdx.Input.Keys.UP;
import static com.badlogic.gdx.Input.Keys.W;

/**
 * Created by Aksi on 21.03.2016.
 */
public class PlayerController implements InputProcessor {

    private MovementControlStyle movementControlStyle;
    HashSet<Integer> pressedKeys;
    ArrayList<Character> printedCharacters;
    HashSet<Integer> usedButtons;
    public Player player;
    Body body;
    Level stage;
    HUD hud;
    public Direction playerDirection = Direction.RIGHT;
    public ImageActor achivmentImage = new ImageActor(getInstance().textureRegions.get("empty"),8.75f,0.25f,7f,1.2f);
    //Текст
    FreeTypeFontGenerator generator = new FreeTypeFontGenerator(getInstance().pathLoader("AABebas.ttf"));
    BitmapFont font;
    public FontActor achivmentText;
    public PlayerController(Level stage, HUD hud) {
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 40;
        parameter.color = Color.BLACK;
        parameter.shadowOffsetX = 1;
        parameter.shadowOffsetY = 2;
        font = generator.generateFont(parameter);
        achivmentText = new FontActor("", 9.3f, 1.1f, font);
        this.stage = stage;
        this.hud = hud;
        this.player = stage.getPlayer();
        body = player.getBody();
        pressedKeys = new HashSet<Integer>();
        printedCharacters = new ArrayList<Character>();
        movementControlStyle = GravityRunner.movementControlStyle;
        usedButtons = new HashSet<Integer>();
        usedButtons.add(UP);
        usedButtons.add(DOWN);
        usedButtons.add(RIGHT);
        usedButtons.add(LEFT);
        usedButtons.add(NUM_1);
        usedButtons.add(NUM_2);
        usedButtons.add(NUM_3);
        usedButtons.add(R);
        usedButtons.add(SPACE);
        usedButtons.add(W);
        usedButtons.add(A);
        usedButtons.add(S);
        usedButtons.add(D);
        usedButtons.add(E);
        usedButtons.add(ESCAPE);
        hud.addActor(achivmentImage);
        hud.addActor(achivmentText);
    }

    public void update() {
        if (FiveSecsOfAchiv>0){
            FiveSecsOfAchiv--;
            if(FiveSecsOfAchiv==1){
                achivmentImage.img = getInstance().textureRegions.get("empty");
                achivmentText.setString("");
            }
        }
        if (keyDown(ESCAPE)&& timerLVL1>0) {
            if(gamePaused == State.NO) {
                gamePaused = State.YES;
            }
            else{
                gamePaused = State.NO;
            }
        }
        if (gamePaused == State.NO) {
            stage.getPhysicsWorld().setGravity(new Vector2(0, G));
            //Буст игрока с платформы для прыжка
            if(jumpboost == State.YES){
                stage.getPlayer().getBody().setLinearVelocity(stage.getPlayer().getBody().getLinearVelocity().x, 30f);
                jumps++;
                if(jumps>=50 && !FiftyJumps){
                    FiftyJumps = true;
                    achivmentGet("Achivment - FiftyJumps!");
                }
                jumpboost = State.NO;
            }
            //Респаун игрока при выпадении из мира
            if (stage.getPlayer().getY() < 0) {
                stage.getPlayer().getBody().setTransform(startX, startY, 0);
                stage.getPlayer().getBody().setLinearVelocity(0, -1f);
                timerLVL1 = 0;
            }
            //Принудительный респаун
            /*if (keyDown(R)) {

            }*/

            //Движение влево/вправо
            switch (getDirection()) {
                case LEFT:
                    if (playerDirection == Direction.RIGHT) {
                        player.setSize(-player.getWidth(), player.getHeight());
                        player.getBody().getFixtureList().removeIndex(3);
                        player.playerHand.shape = new PolygonShape();
                        ((PolygonShape) player.playerHand.shape).setAsBox(CHAR_TILE_SIZE * getInstance().getPpuX() / 10, CHAR_TILE_SIZE * getInstance().getPpuX() / 10, new Vector2(-1, -0.2f), 0);
                        body.createFixture(player.playerHand).setUserData("playerHand");
                        player.playerHand.shape.dispose();
                    }
                    playerDirection = Direction.LEFT;
                    if (body.getLinearVelocity().x > -10f) {
                        if (planet == Planet.MOON)
                            body.setLinearVelocity(body.getLinearVelocity().x - 0.2f, body.getLinearVelocity().y);
                        if (planet == Planet.EARTH)
                            body.setLinearVelocity(body.getLinearVelocity().x - 0.25f, body.getLinearVelocity().y);
                        if (planet == Planet.UPITER)
                            body.setLinearVelocity(body.getLinearVelocity().x - 0.3f, body.getLinearVelocity().y);
                    }
                    break;
                case RIGHT:
                    if (playerDirection == Direction.LEFT) {
                        player.setSize(-player.getWidth(), player.getHeight());
                        player.getBody().getFixtureList().removeIndex(3);
                        player.playerHand.shape = new PolygonShape();
                        ((PolygonShape) player.playerHand.shape).setAsBox(CHAR_TILE_SIZE * getInstance().getPpuX() / 10, CHAR_TILE_SIZE * getInstance().getPpuX() / 10, new Vector2(1, -0.2f), 0);
                        body.createFixture(player.playerHand).setUserData("playerHand");
                        player.playerHand.shape.dispose();
                    }
                    playerDirection = Direction.RIGHT;
                    if (body.getLinearVelocity().x < 10f) {
                        if (planet == Planet.MOON)
                            body.setLinearVelocity(body.getLinearVelocity().x + 0.2f, body.getLinearVelocity().y);
                        if (planet == Planet.EARTH)
                            body.setLinearVelocity(body.getLinearVelocity().x + 0.25f, body.getLinearVelocity().y);
                        if (planet == Planet.UPITER)
                            body.setLinearVelocity(body.getLinearVelocity().x + 0.3f, body.getLinearVelocity().y);
                    }
                    break;
            }
            //Приземление
            if (inAir == State.NO)
                player.img = getInstance().textureRegions.get("playerStay");
            switch (getAction()) {
                //Прыжок
                case JUMP:
                    if (inAir == State.NO) {
                        if (planet == Planet.MOON)
                            body.setLinearVelocity(body.getLinearVelocity().x * 1.3f, 4f);
                        if (planet == Planet.EARTH)
                            body.setLinearVelocity(body.getLinearVelocity().x * 1.3f, 6f);
                        if (planet == Planet.UPITER)
                            body.setLinearVelocity(body.getLinearVelocity().x * 1.3f, 7f);
                        inAir = State.YES;
                        player.img = getInstance().textureRegions.get("playerJump");
                        jumps++;
                        if(jumps>=50 && !FiftyJumps){
                            FiftyJumps = true;
                            achivmentGet("Achivment - FiftyJumps!");
                        }
                    }
                    //Ракетный ранец
                    if (planet == Planet.MOON) {
                        body.setLinearVelocity(body.getLinearVelocity().x, body.getLinearVelocity().y + 0.03f);
                    }
                    if (planet == Planet.EARTH) {
                        body.setLinearVelocity(body.getLinearVelocity().x, body.getLinearVelocity().y + 0.22f);
                    }
                    if (planet == Planet.UPITER) {
                        body.setLinearVelocity(body.getLinearVelocity().x, body.getLinearVelocity().y + 0.5f);
                    }
                    break;
                //Рывок вниз
                case FALL:
                    if (planet == Planet.MOON) {
                        body.setLinearVelocity(body.getLinearVelocity().x, body.getLinearVelocity().y - 0.03f);
                    }
                    if (planet == Planet.EARTH) {
                        body.setLinearVelocity(body.getLinearVelocity().x, body.getLinearVelocity().y - 0.25f);
                    }
                    if (planet == Planet.UPITER) {
                        body.setLinearVelocity(body.getLinearVelocity().x, body.getLinearVelocity().y - 0.6f);
                    }
                    break;
                //Активатор
                case PRESS:
                /*if (GravityRunner.nearSwitch == State.YES && switch1 == State.NO) {
                    switch1 = State.YES;
                    stage.addWall(GravityRunner.getInstance().textureRegions.get("black2"), 12, 5, stage.getPhysicsWorld(), "01");
                    stage.addWall(GravityRunner.getInstance().textureRegions.get("black2"), 14, 5, stage.getPhysicsWorld(), "01");
                    stage.addWall(GravityRunner.getInstance().textureRegions.get("black2"), 16, 5, stage.getPhysicsWorld(), "01");
                }*/
                    break;

            }
        }
    }

    public boolean keyDown(int key) {
        return usedButtons.contains(key) && pressedKeys.add(key);
    }

    public boolean keyUp(int key) {
        return usedButtons.contains(key) && pressedKeys.remove(key);
    }

    @Override
    public boolean keyTyped(char character) {
        return printedCharacters.add(character);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        //pressedPointers.put(pointer, new Pointer(screenX, screenY, button));
        if (gamePaused == State.YES)
        {
            Actor actor = hud.hit(screenX, screenY, true);
            if(actor.getName()!=null) {
                if (actor.getName().equals("PlayBtn")) {
                    gamePaused = State.NO;
                }
                if (actor.getName().equals("MenuBtn")) {
                    gamePaused = State.NO;
                    getInstance().showMenu();
                    timerLVL1 = 0;
                    getInstance().gameScreen.dispose();
                }
                if (actor.getName().equals("ResetBtn")) {
                    try {
                        stage.restartLvl();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    gamePaused = State.NO;
                    stage.getPlayer().getBody().setTransform(startX, startY, 0);
                    stage.getPlayer().getBody().setLinearVelocity(0, -1f);
                    System.out.println("Respawn!");
                    timerLVL1 = 0;
                }
            }
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        //if (!pressedPointers.containsKey(pointer))
        //    return false;
        //pressedPointers.remove(pointer);
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        //if (!pressedPointers.containsKey(pointer))
        //    return false;
        //pressedPointers.get(pointer).setPosition(screenX, screenY);
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public PlayerAction getAction() {
        switch (movementControlStyle) {
            case BUTTONS:
                if (pressedKeys.contains(DOWN) || pressedKeys.contains(S)) {
                    return PlayerAction.FALL;
                }
                if (pressedKeys.contains(UP) || pressedKeys.contains(W) || pressedKeys.contains(SPACE)) {
                    return PlayerAction.JUMP;
                }
                if (pressedKeys.contains(LEFT) || pressedKeys.contains(RIGHT) || pressedKeys.contains(A)|| pressedKeys.contains(D)) {
                    return PlayerAction.MOVE;
                }
                if (keyDown(E)||keyDown(ENTER)) {
                    return PlayerAction.PRESS;
                }
        }
        return PlayerAction.NONE;
    }

    public Direction getDirection() {
        switch (movementControlStyle) {
            case BUTTONS:
                if (pressedKeys.contains(Input.Keys.RIGHT) || pressedKeys.contains(Input.Keys.D)) {
                    return Direction.RIGHT;
                }
                if (pressedKeys.contains(Input.Keys.LEFT) || pressedKeys.contains(Input.Keys.A)) {
                    return Direction.LEFT;
                }
        }
        return Direction.NONE;
    }
    public void achivmentGet(String achivment){
        achivmentImage.img = getInstance().textureRegions.get("Achivment");
        achivmentText.setString(achivment);
        FiveSecsOfAchiv = 300;
    }
}
