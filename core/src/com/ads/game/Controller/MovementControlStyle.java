package com.ads.game.Controller;

/**
 * Created by Aksi on 15.04.2016.
 */
public enum MovementControlStyle {
    TILT,
    STEER,
    TOUCH,
    BUTTONS,
    NONE
}