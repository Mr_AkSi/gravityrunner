package com.ads.game.Controller;

/**
 * Created by Aksi on 21.03.2016.
 */
public enum PlayerAction {
    STAND,
    MOVE,
    JUMP,
    NONE,
    FALL,
    PRESS
}
