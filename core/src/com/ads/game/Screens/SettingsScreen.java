package com.ads.game.Screens;

import com.ads.game.Controller.BarState;
import com.ads.game.Controller.ToggleState;
import com.ads.game.GravityRunner;
import com.ads.game.View.BarActor;
import com.ads.game.View.ImageActor;
import com.ads.game.View.ToggleActor;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragScrollListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Created by Aksi on 25.01.2016.
 */

public class SettingsScreen implements Screen {
    private Stage stage;
    private ToggleActor musicToggleButton;
    private BarActor musicBar;
    private ToggleActor soundToggleButton;
    private BarActor soundBar;
    private ToggleActor brightnessToggleButton;
    private BarActor brightnessBar;
    private BarState musicLevel;
    private BarState soundLevel;
    private BarState brightnessLevel;


    public SettingsScreen(SpriteBatch batch) {
        ImageActor backGround = new ImageActor(GravityRunner.getInstance().textureRegions.get("background"), 0f, 0f, 16f, 9f);
        //Кнопка "назад"
        ImageActor backButton = new ImageActor(GravityRunner.getInstance().textureRegions.get("MenuBtn"), 1f, 3.25f, 2.5f, 2.5f);
        backButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                GravityRunner.getInstance().showMenu();
            }
        });
        //Музыка
        musicToggleButton = new ToggleActor(GravityRunner.getInstance().textureRegions.get("MusicOnBtn"), GravityRunner.getInstance().textureRegions.get("MusicOffBtn"), 4.25f, 1.25f, 2f, 2f);
        musicToggleButton.setState(GravityRunner.getInstance().getMusicState());
        musicToggleButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                musicToggleButton.toggle();
                GravityRunner.getInstance().setSoundState(musicToggleButton.getState());
                if (musicToggleButton.getState() == ToggleState.OFF) {
                    musicLevel = musicBar.getState();
                    musicBar.setState(BarState.BAR0);
                } else {
                    musicBar.setState(musicLevel);
                }
            }
        });

        musicBar = new BarActor(6.75f, 1.5f, 8.5f, 1.5f);
        musicBar.setState(GravityRunner.getInstance().getMusicBarState());
        musicBar.addListener(new DragListener() {
            public void touchDragged (InputEvent event, float x, float y, int pointer) {
                if (x > 0) musicBar.setState(BarState.BAR1);
                if (x > (musicBar.getWidth() / 7)) musicBar.setState(BarState.BAR2);
                if (x > (musicBar.getWidth() / 7) * 2) musicBar.setState(BarState.BAR3);
                if (x > (musicBar.getWidth() / 7) * 3) musicBar.setState(BarState.BAR4);
                if (x > (musicBar.getWidth() / 7) * 4) musicBar.setState(BarState.BAR5);
                if (x > (musicBar.getWidth() / 7) * 5) musicBar.setState(BarState.BAR6);
                if (x > (musicBar.getWidth() / 7) * 6) musicBar.setState(BarState.BAR7);
                musicLevel = musicBar.getState();
                if (musicToggleButton.getState() == ToggleState.OFF) musicToggleButton.toggle();
            }
        });
        GravityRunner.getInstance().setMusicBarState(musicBar.getState());
        //Звук
        soundToggleButton = new ToggleActor(GravityRunner.getInstance().textureRegions.get("SoundOnBtn"), GravityRunner.getInstance().textureRegions.get("SoundOffBtn"), 4.25f, 3.5f, 2f, 2f);
        soundToggleButton.setState(GravityRunner.getInstance().getSoundState());
        soundToggleButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                soundToggleButton.toggle();
                GravityRunner.getInstance().setSoundState(soundToggleButton.getState());
                if (soundToggleButton.getState() == ToggleState.OFF) {
                    soundLevel = soundBar.getState();
                    soundBar.setState(BarState.BAR0);
                } else {
                    soundBar.setState(soundLevel);
                }
            }
        });

        soundBar = new BarActor(6.75f, 3.75f, 8.5f, 1.5f);
        soundBar.setState(GravityRunner.getInstance().getSoundBarState());
        soundBar.addListener(new DragListener() {
            public void touchDragged (InputEvent event, float x, float y, int pointer) {
                if (x > 0) soundBar.setState(BarState.BAR1);
                if (x > (soundBar.getWidth() / 7)) soundBar.setState(BarState.BAR2);
                if (x > (soundBar.getWidth() / 7) * 2) soundBar.setState(BarState.BAR3);
                if (x > (soundBar.getWidth() / 7) * 3) soundBar.setState(BarState.BAR4);
                if (x > (soundBar.getWidth() / 7) * 4) soundBar.setState(BarState.BAR5);
                if (x > (soundBar.getWidth() / 7) * 5) soundBar.setState(BarState.BAR6);
                if (x > (soundBar.getWidth() / 7) * 6) soundBar.setState(BarState.BAR7);
                soundLevel = soundBar.getState();
                if (soundToggleButton.getState() == ToggleState.OFF) soundToggleButton.toggle();
            }
        });
        GravityRunner.getInstance().setSoundBarState(soundBar.getState());
        //Яркость экрана
        brightnessToggleButton = new ToggleActor(GravityRunner.getInstance().textureRegions.get("BrightnessOnBtn"), GravityRunner.getInstance().textureRegions.get("BrightnessOffBtn"), 4.25f, 5.75f, 2f, 2f);
        brightnessToggleButton.setState(GravityRunner.getInstance().getBrightnessState());
        brightnessToggleButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                brightnessToggleButton.toggle();
                GravityRunner.getInstance().setSoundState(brightnessToggleButton.getState());
                if (brightnessToggleButton.getState() == ToggleState.OFF) {
                    brightnessLevel = brightnessBar.getState();
                    brightnessBar.setState(BarState.BAR0);
                } else {
                    brightnessBar.setState(brightnessLevel);
                }
            }
        });

        brightnessBar = new BarActor(6.75f, 6f, 8.5f, 1.5f);
        brightnessBar.setState(GravityRunner.getInstance().getBrightnessBarState());
        brightnessBar.addListener(new DragListener() {
            public void touchDragged (InputEvent event, float x, float y, int pointer) {
                if (x > 0) brightnessBar.setState(BarState.BAR1);
                if (x > (brightnessBar.getWidth() / 7)) brightnessBar.setState(BarState.BAR2);
                if (x > (brightnessBar.getWidth() / 7) * 2) brightnessBar.setState(BarState.BAR3);
                if (x > (brightnessBar.getWidth() / 7) * 3) brightnessBar.setState(BarState.BAR4);
                if (x > (brightnessBar.getWidth() / 7) * 4) brightnessBar.setState(BarState.BAR5);
                if (x > (brightnessBar.getWidth() / 7) * 5) brightnessBar.setState(BarState.BAR6);
                if (x > (brightnessBar.getWidth() / 7) * 6) brightnessBar.setState(BarState.BAR7);
                brightnessLevel = brightnessBar.getState();
                if (brightnessToggleButton.getState() == ToggleState.OFF)
                    brightnessToggleButton.toggle();
            }
        });
        GravityRunner.getInstance().setBrightnessBarState(brightnessBar.getState());
        //Сцена


        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);
        stage.addActor(backGround);
        stage.addActor(backButton);
        stage.addActor(musicToggleButton);
        stage.addActor(musicBar);
        stage.addActor(soundBar);
        stage.addActor(brightnessToggleButton);
        stage.addActor(brightnessBar);
        stage.addActor(soundToggleButton);


    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }
}
