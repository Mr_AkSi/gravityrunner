package com.ads.game.Screens;

import com.ads.game.Controller.PlayerController;
import com.ads.game.Controller.State;
import com.ads.game.GravityRunner;
import com.ads.game.Model.BG;
import com.ads.game.Model.HUD;
import com.ads.game.Model.Level;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.util.HashMap;

import javax.swing.KeyStroke;
import javax.xml.crypto.dsig.keyinfo.KeyName;

import static com.badlogic.gdx.Input.Keys.ESCAPE;

/**
 * Created by Aksi on 11.01.2016.
 */
public class GameScreen implements Screen {
    BG background;
    public Level world;
    public HUD HUD;
    public InputMultiplexer multiplexer;
    public PlayerController playerController;
    OrthographicCamera BGcam;
    OrthographicCamera cam;
    OrthographicCamera HudCam;
    Box2DDebugRenderer debugRenderer;
    SpriteBatch spriteBatch;
    public int levelNum = 1;
    public GameScreen(SpriteBatch batch) throws FileNotFoundException {
        BGcam = new OrthographicCamera();
        cam = new OrthographicCamera();
        HudCam = new OrthographicCamera();
        BGcam.setToOrtho(false, GravityRunner.WORLD_WIDTH, GravityRunner.WORLD_HEIGHT);
        cam.setToOrtho(false, GravityRunner.WORLD_WIDTH, GravityRunner.WORLD_HEIGHT);
        HudCam.setToOrtho(false, GravityRunner.WORLD_WIDTH, GravityRunner.WORLD_HEIGHT);
        background = new BG(new ScreenViewport(BGcam),batch);
        world = new Level(new ScreenViewport(cam), batch, cam, GravityRunner.getInstance().level1);
        HUD = new HUD(new ScreenViewport(HudCam),batch);
        playerController = new PlayerController(world, HUD);
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(world);
        multiplexer.addProcessor(playerController);
        multiplexer.addProcessor(HUD);
        debugRenderer = new Box2DDebugRenderer();
        spriteBatch = batch;
        debugRenderer.SHAPE_STATIC.set(Color.BLUE);
        debugRenderer.SHAPE_AWAKE.set(Color.WHITE);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        playerController.update();
        background.act();
        background.draw();
        world.act(delta, cam);
        world.draw();
        HUD.act();
        HUD.draw();
        if (GravityRunner.PHYSICS_DEBUG) {
            spriteBatch.begin();
            debugRenderer.render(world.getPhysicsWorld(), cam.combined);
            spriteBatch.end();
        }

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        world.dispose();
    }
}
