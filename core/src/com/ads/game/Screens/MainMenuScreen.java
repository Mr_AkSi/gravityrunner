package com.ads.game.Screens;

import com.ads.game.Controller.State;
import com.ads.game.GravityRunner;
import com.ads.game.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;


/**
 * Created by Aksi on 11.01.2016.
 */
public class MainMenuScreen implements Screen {
    private Stage stage;
    private com.ads.game.View.ImageActor logo;
    private com.ads.game.View.ImageActor playButton;
    private com.ads.game.View.ImageActor settingsButton;
    private com.ads.game.View.ImageActor statsButton;
    private com.ads.game.View.ImageActor backGround;
    private Viewport viewport;
    private OrthographicCamera camera;

    public MainMenuScreen(SpriteBatch batch) {
        initStage(batch);
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        viewport = new StretchViewport(GravityRunner.WORLD_WIDTH, GravityRunner.WORLD_HEIGHT, camera);
        backGround = new ImageActor(GravityRunner.getInstance().textureRegions.get("background"), 0f, 0f, 16f, 9f);
        logo = new ImageActor(GravityRunner.getInstance().textureRegions.get("logo"), 4f, 4f, 8f, 4f);
        playButton = new ImageActor(GravityRunner.getInstance().textureRegions.get("PlayBtn"), 6.5f, 0.5f, 3f, 3f);
        playButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                GravityRunner.getInstance().showLevels();
                GravityRunner.gamePaused = State.NO;
            }
        });
        statsButton = new ImageActor(GravityRunner.getInstance().textureRegions.get("stats"), 4f, 1f, 2f, 2f);
        statsButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                GravityRunner.getInstance().showStats();
            }
        });
        settingsButton = new ImageActor(GravityRunner.getInstance().textureRegions.get("settings"), 10f, 1f, 2f, 2f);
        settingsButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                GravityRunner.getInstance().showSettings();
            }
        });

        stage.addActor(backGround);
        stage.addActor(logo);
        stage.addActor(playButton);
        stage.addActor(settingsButton);
        stage.addActor(statsButton);


    }

    private void initStage(SpriteBatch batch) {
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }
}