package com.ads.game.Screens;

import com.ads.game.Controller.Planet;
import com.ads.game.GravityRunner;
import com.ads.game.View.FontActor;
import com.ads.game.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.io.FileNotFoundException;

import static com.ads.game.GravityRunner.getInstance;

/**
 * Created by Aksi on 25.04.2016.
 */
public class DifficultyScreen implements Screen {
    private Stage stage;
    private ImageActor backGround;
    private ImageActor backButton;
    private ImageActor easyButton;
    private ImageActor mediumButton;
    private ImageActor hardButton;
    private FontActor Difficulty;

    public DifficultyScreen(SpriteBatch batch) {
        backGround = new ImageActor(getInstance().textureRegions.get("background"), 0f, 0f, 16f, 9f);
        easyButton = new ImageActor(getInstance().textureRegions.get("easy"), 4.5f, 4.5f, 7f, 1.2f);
        easyButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {

                try {
                    getInstance().gameScreen = new GameScreen(getInstance().batch);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                GravityRunner.planet = Planet.MOON;
                GravityRunner.G = -3.25f;
                getInstance().showGame();
            }
        });
        mediumButton = new ImageActor(getInstance().textureRegions.get("medium"), 4.5f, 3f, 7f, 1.2f);
        mediumButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                try {
                    getInstance().gameScreen = new GameScreen(getInstance().batch);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                GravityRunner.planet = Planet.EARTH;
                GravityRunner.G = -19.6f;
                getInstance().showGame();
            }
        });
        hardButton = new ImageActor(getInstance().textureRegions.get("hard"), 4.5f, 1.5f, 7f, 1.2f);
        hardButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                try {
                    getInstance().gameScreen = new GameScreen(getInstance().batch);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                GravityRunner.planet = Planet.UPITER;
                GravityRunner.G = -49.9f;
                getInstance().showGame();
            }
        });
        backButton = new ImageActor(getInstance().textureRegions.get("MenuBtn"), 1f, 1f, 2f, 2f);
        backButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                getInstance().showMenu();
            }
        });
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(getInstance().pathLoader("AABebas.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter1 = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter1.size = 100;
        parameter1.color = Color.WHITE;
        parameter1.shadowOffsetX = 3;
        parameter1.shadowOffsetY = 5;
        BitmapFont font1 = generator.generateFont(parameter1);
        Difficulty = new FontActor("Choose difficulty", 3.5f, 7.5f, font1);
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);
        stage.addActor(backGround);
        stage.addActor(easyButton);
        stage.addActor(mediumButton);
        stage.addActor(hardButton);
        stage.addActor(backButton);
        stage.addActor(Difficulty);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }
}
