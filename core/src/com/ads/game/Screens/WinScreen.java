package com.ads.game.Screens;

import com.ads.game.Controller.State;
import com.ads.game.GravityRunner;
import com.ads.game.Model.Level;
import com.ads.game.View.FontActor;
import com.ads.game.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.io.FileNotFoundException;

/**
 * Created by LeftCaps on 23.05.2016.
 */
public class WinScreen implements Screen {
    FreeTypeFontGenerator generator = new FreeTypeFontGenerator(GravityRunner.getInstance().pathLoader("MicraNormal.ttf"));
    Stage stage;
    ImageActor backGround;
    ImageActor backButton;
    BitmapFont font;
    ImageActor timerOvrl;
    FontActor timerText;
    FontActor top3, Stats;
    String hours = "00";
    String minutes = "00";
    String seconds = "00";
    String miliseconds = "0";
    public int LevelsComplete = 0, Place = 1, Achivments = 0;

    public WinScreen(SpriteBatch batch){
        backGround = new ImageActor(GravityRunner.getInstance().textureRegions.get("background"), 0f, 0f, 16f, 9f);
        backButton = new ImageActor(GravityRunner.getInstance().textureRegions.get("MenuBtn"), 3f, 0.5f, 2f, 2f);
        backButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                GravityRunner.getInstance().showMenu();
            }
        });
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
