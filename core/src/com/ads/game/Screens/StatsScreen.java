package com.ads.game.Screens;

import com.ads.game.GravityRunner;
import com.ads.game.View.FontActor;
import com.ads.game.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Created by Aksi on 15.02.2016.
 */
public class StatsScreen implements Screen {
    Stage stage;
    ImageActor backGround;
    ImageActor backButton;
    ImageActor top3texture;
    FontActor top3, Stats;
    String hours = "00";
    String minutes = "00";
    String seconds = "00";
    String miliseconds = "0";
    public int LevelsComplete = 0, Place = 1, Achivments = 0, TotalTime, Level1Time = 1000000, Level2Time = 1000000, Level3Time = 1000000, Level4Time = 1000000, Level5Time = 1000000, Level6Time = 1000000, Level7Time = 1000000, Level8Time = 1000000;

    public StatsScreen(SpriteBatch batch) {
        backGround = new ImageActor(GravityRunner.getInstance().textureRegions.get("background"), 0f, 0f, 16f, 9f);
        backButton = new ImageActor(GravityRunner.getInstance().textureRegions.get("MenuBtn"), 3f, 0.5f, 2f, 2f);
        top3texture = new ImageActor(GravityRunner.getInstance().textureRegions.get("top3"), 2f, 0.5f, 14f, 8.2f);
        backButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                GravityRunner.getInstance().showMenu();
            }
        });
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(GravityRunner.getInstance().pathLoader("AABebas.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter1 = new FreeTypeFontGenerator.FreeTypeFontParameter();
        FreeTypeFontGenerator.FreeTypeFontParameter parameter2 = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter1.size = 170;
        parameter1.color = Color.WHITE;
        parameter1.shadowOffsetX = 3;
        parameter1.shadowOffsetY = 5;
        parameter2.size = 50;
        parameter2.color = Color.WHITE;
        parameter2.shadowOffsetX = 3;
        parameter2.shadowOffsetY = 5;
        BitmapFont font1 = generator.generateFont(parameter1);
        BitmapFont font2 = generator.generateFont(parameter2);
        top3 = new FontActor("TOP-3", 9.5f, 8f, font1);
        Stats = new FontActor("Your stats:\r\nLevels completed: " + LevelsComplete + "\r\nTotal time: " + minutes + ":" + seconds + ":" + miliseconds + "\r\nPlace: " + Place + "\r\nAchivments: " + Achivments + "/10\r\n", 1f, 7f, font2);
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);
        stage.addActor(backGround);
        stage.addActor(top3texture);
        stage.addActor(backButton);
        stage.addActor(top3);
        stage.addActor(Stats);
    }

    @Override
    public void render(float delta) {
        setAchivments();
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
        Stats.setString("Your stats:\r\nLevels complete: " + LevelsComplete + "\r\nTotal time: " + hours + ":" + minutes + ":" + seconds + ":" + miliseconds + "\r\nPlace: " + Place + "\r\nAchivments: " + Achivments + "/50\r\n");
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    public void setTimer() {
        hours = GravityRunner.TotalTime / 216000 + "";
        if (hours.length() < 2) hours = "0" + hours;
        minutes = GravityRunner.TotalTime / 3600 + "";
        if (minutes.length() < 2) minutes = "0" + minutes;
        seconds = GravityRunner.TotalTime % 3600 / 60 + "";
        if (seconds.length() < 2) seconds = "0" + seconds;
        miliseconds = GravityRunner.TotalTime % 60 / 6 + "";
    }

    public void setStats() {
        LevelsComplete = 0;
        TotalTime = 0;
        if (GravityRunner.Level1Completed) {
            LevelsComplete++;
            if (Level1Time > GravityRunner.timerLVL1) Level1Time = GravityRunner.timerLVL1;
            TotalTime += Level1Time;
        }
        if (GravityRunner.Level2Completed) {
            LevelsComplete++;
            if (Level2Time > GravityRunner.timerLVL2) Level2Time = GravityRunner.timerLVL2;
            TotalTime += Level2Time;
        }
        if (GravityRunner.Level3Completed) {
            LevelsComplete++;
            if (Level3Time > GravityRunner.timerLVL3) Level3Time = GravityRunner.timerLVL3;
            TotalTime += Level3Time;
        }
        if (GravityRunner.Level4Completed) {
            LevelsComplete++;
            if (Level4Time > GravityRunner.timerLVL4) Level4Time = GravityRunner.timerLVL4;
            TotalTime += Level4Time;
        }
        if (GravityRunner.Level5Completed) {
            LevelsComplete++;
            if (Level5Time > GravityRunner.timerLVL5) Level5Time = GravityRunner.timerLVL5;
            TotalTime += Level5Time;
        }
        if (GravityRunner.Level6Completed) {
            LevelsComplete++;
            if (Level6Time > GravityRunner.timerLVL6) Level6Time = GravityRunner.timerLVL6;
            TotalTime += Level6Time;
        }
        if (GravityRunner.Level7Completed) {
            LevelsComplete++;
            if (Level7Time > GravityRunner.timerLVL7) Level7Time = GravityRunner.timerLVL7;
            TotalTime += Level7Time;
        }
        if (GravityRunner.Level8Completed) {
            LevelsComplete++;
            if (Level8Time > GravityRunner.timerLVL8) Level8Time = GravityRunner.timerLVL8;
            TotalTime += Level8Time;
        }

    }

    public void setAchivments() {
        Achivments = 0;
        if (GravityRunner.FiftyJumps) Achivments++;
    }
}
