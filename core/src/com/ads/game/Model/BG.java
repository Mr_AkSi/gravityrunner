package com.ads.game.Model;

import com.ads.game.Controller.Planet;
import com.ads.game.Controller.State;
import com.ads.game.GravityRunner;
import com.ads.game.View.FontActor;
import com.ads.game.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Created by Aksi on 25.04.2016.
 */
public class BG extends Stage {
    private ImageActor bg;
       public BG(ScreenViewport screenViewport, SpriteBatch batch) {
           super(screenViewport, batch);
           bg = new ImageActor(GravityRunner.getInstance().textureRegions.get("moon"), 0f, 0f, 16f, 9f);
           addActor(bg);
       }
    public void act() {
        switch (GravityRunner.planet) {
            case MOON:
                bg.img = GravityRunner.getInstance().textureRegions.get("moon");
                break;
            case EARTH:
                bg.img = GravityRunner.getInstance().textureRegions.get("earth");
                break;
            case UPITER:
                bg.img = GravityRunner.getInstance().textureRegions.get("upiter");
                break;
        }
    }
}

