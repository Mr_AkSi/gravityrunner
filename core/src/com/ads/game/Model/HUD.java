package com.ads.game.Model;

import com.ads.game.Controller.State;
import com.ads.game.GravityRunner;
import com.ads.game.View.FontActor;
import com.ads.game.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Created by Aksi on 25.04.2016.
 */
public class HUD extends Stage {
    FreeTypeFontGenerator generator = new FreeTypeFontGenerator(GravityRunner.getInstance().pathLoader("MicraNormal.ttf"));
    FontActor timerText;
    String seconds;
    String minutes;
    String miliseconds;
    BitmapFont font;
    ImageActor timerOvrl;
    private ImageActor PauseBG;
    private ImageActor BackBtn;
    private ImageActor menuBtn;
    private ImageActor resetBtn;
    SpriteBatch batch;
    public HUD(ScreenViewport screenViewport, SpriteBatch batch){
        super(screenViewport, batch);
        this.batch = batch;
        PauseBG = new ImageActor(GravityRunner.getInstance().textureRegions.get("pauseBG"), 0f, 0f, GravityRunner.WORLD_WIDTH, GravityRunner.WORLD_HEIGHT);
        menuBtn = new ImageActor(GravityRunner.getInstance().textureRegions.get("MenuBtn"), 4.5f, 3.7f, 2f, 2f, "MenuBtn");
        resetBtn = new ImageActor(GravityRunner.getInstance().textureRegions.get("ResetBtn"), 7f, 3.7f, 2f, 2f, "ResetBtn");
        BackBtn = new ImageActor(GravityRunner.getInstance().textureRegions.get("PlayBtn"), 9.5f, 3.7f, 2f, 2f, "PlayBtn");
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 50;
        parameter.color = Color.WHITE;
        parameter.shadowOffsetX = 3;
        parameter.shadowOffsetY = 5;
        font = generator.generateFont(parameter);
        timerOvrl = new ImageActor(GravityRunner.getInstance().textureRegions.get("timer"), 0, 0, GravityRunner.WORLD_WIDTH,GravityRunner.WORLD_HEIGHT);
        timerText = new FontActor(GravityRunner.timerLVL1 / 3600 + ":" + GravityRunner.timerLVL1 % 3600 / 60 + ":" + GravityRunner.timerLVL1 % 60, 6f, 8.8f, font);
        addActor(timerOvrl);
        addActor(timerText);
        addActor(PauseBG);
        addActor(BackBtn);
        addActor(menuBtn);
        addActor(resetBtn);

    }
    public void act() {
        setTimer();
        if(GravityRunner.gamePaused == State.NO) {
            GravityRunner.timerLVL1++;
            PauseBG.img = GravityRunner.getInstance().textureRegions.get("empty");
            BackBtn.img = GravityRunner.getInstance().textureRegions.get("empty");
            menuBtn.img = GravityRunner.getInstance().textureRegions.get("empty");
            resetBtn.img = GravityRunner.getInstance().textureRegions.get("empty");
        }
        else
        {
            PauseBG.img = GravityRunner.getInstance().textureRegions.get("pauseBG");
            BackBtn.img = GravityRunner.getInstance().textureRegions.get("PlayBtn");
            menuBtn.img = GravityRunner.getInstance().textureRegions.get("MenuBtn");
            resetBtn.img = GravityRunner.getInstance().textureRegions.get("ResetBtn");
        }
        timerText.setString(minutes+":"+seconds+":"+miliseconds);

    }
    public void setTimer(){
        minutes = GravityRunner.timerLVL1 / 3600+"";
        if(minutes.length()<2)minutes = "0"+minutes;
        seconds = GravityRunner.timerLVL1 % 3600 / 60+"";
        if(seconds.length()<2)seconds = "0"+seconds;
        miliseconds = GravityRunner.timerLVL1 %60/6+"";
    }
}

