package com.ads.game.Model;

import com.ads.game.GravityRunner;
import com.ads.game.View.ImageActor;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.DynamicBody;

/**
 * Created by Aksi on 29.03.2016.
 */
public class PhysicsBox extends ImageActor {
    TextureRegion img;
    Body body;
    FixtureDef fixtureDef;
    public PhysicsBox(TextureRegion img, float x, float y, float w, float h, World world) {
        this.img = img;
        setSize(w*GravityRunner.OUTPUT_TILE_SIZE * GravityRunner.getInstance().getPpuX(), h*GravityRunner.OUTPUT_TILE_SIZE * GravityRunner.getInstance().getPpuY());
        BodyDef bodyDef = new BodyDef();
        bodyDef.fixedRotation = false;

        bodyDef.position.x = x + getWidth() / 2;
        bodyDef.position.y = y + getHeight() / 2;
        bodyDef.type = DynamicBody;

        body = world.createBody(bodyDef);

        fixtureDef = new FixtureDef();
        fixtureDef.shape = new PolygonShape();
        ((PolygonShape) fixtureDef.shape).setAsBox(getWidth() / 2, getHeight() / 2);
        fixtureDef.friction = 0.8f;
        fixtureDef.density = 0.2f;
        fixtureDef.restitution = 0f;
        body.createFixture(fixtureDef).setUserData("wall");
        fixtureDef.shape.dispose();
    }

    public void act(float delta) {
        setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - getHeight() / 2);

    }

    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY(), getWidth()/2, getHeight()/2, getWidth(), getHeight(), getScaleX(), getScaleY(), body.getAngle()*57f);
        //Почему 57??
    }
}
