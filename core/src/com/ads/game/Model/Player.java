package com.ads.game.Model;

import com.ads.game.Controller.PlayerController;
import com.ads.game.GravityRunner;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;

import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.DynamicBody;

/**
 * Created by Aksi on 29.03.2016.
 */

public class Player extends Actor {
    public TextureRegion img;
    public Body body;
    private World world;
    private OrthographicCamera camera;
    private PlayerController playerController;
    public BodyDef bodyDef;
    public FixtureDef playerFeet;
    public FixtureDef playerLegs;
    public FixtureDef playerBody;
    public FixtureDef playerHand;


    public TextureRegion getImg() {
        return img;
    }

    public Player(TextureRegion img, float x, float y, World world) {
        this.world = world;
        this.img = img;
        setPosition(x, y);
        setSize(-GravityRunner.CHAR_TILE_SIZE * GravityRunner.getInstance().getPpuX(), GravityRunner.CHAR_TILE_SIZE * GravityRunner.getInstance().getPpuY());

        bodyDef = new BodyDef();
        bodyDef.fixedRotation = true;
        bodyDef.position.x = x;
        bodyDef.position.y = y;
        bodyDef.type = DynamicBody;
        body = world.createBody(bodyDef);

        playerBody = new FixtureDef();
        playerFeet = new FixtureDef();
        playerHand = new FixtureDef();
        playerLegs = new FixtureDef();
        playerFeet.shape = new PolygonShape();
        playerBody.shape = new PolygonShape();
        playerHand.shape = new PolygonShape();
        playerLegs.shape = new PolygonShape();
        playerHand.isSensor = true;
        ((PolygonShape) playerBody.shape).setAsBox(GravityRunner.CHAR_TILE_SIZE * GravityRunner.getInstance().getPpuX() / 5f, GravityRunner.CHAR_TILE_SIZE * GravityRunner.getInstance().getPpuY() / 4f, new Vector2(0, 0.5f), 0);
        ((PolygonShape) playerFeet.shape).set(new Vector2[]{
                //Левый верхний
                new Vector2(-GravityRunner.CHAR_TILE_SIZE * GravityRunner.getInstance().getPpuX() / 6.5f, -getHeight() / 3f),
                //Левый нижний
                new Vector2(-GravityRunner.CHAR_TILE_SIZE * GravityRunner.getInstance().getPpuX() / 10f, -getHeight() / 2.4f),
                //Правый нижний
                new Vector2(GravityRunner.CHAR_TILE_SIZE * GravityRunner.getInstance().getPpuX() / 10f, -getHeight() / 2.4f),
                //Правый верхний
                new Vector2(GravityRunner.CHAR_TILE_SIZE * GravityRunner.getInstance().getPpuX() / 6.5f, -getHeight() / 3f),
        });
        ((PolygonShape) playerLegs.shape).set(new Vector2[]{
                //Левый верхний
                new Vector2(-GravityRunner.CHAR_TILE_SIZE * GravityRunner.getInstance().getPpuX() / 5f, -getHeight() / 7f),
                //Левый нижний
                new Vector2(-GravityRunner.CHAR_TILE_SIZE * GravityRunner.getInstance().getPpuX() / 6.5f, -getHeight() / 3f),
                //Правый нижний
                new Vector2(GravityRunner.CHAR_TILE_SIZE * GravityRunner.getInstance().getPpuX() / 6.5f, -getHeight() / 3f),
                //Правый верхний
                new Vector2(GravityRunner.CHAR_TILE_SIZE * GravityRunner.getInstance().getPpuX() / 5f, -getHeight() / 7f),
        });
        ((PolygonShape) playerHand.shape).setAsBox(GravityRunner.CHAR_TILE_SIZE * GravityRunner.getInstance().getPpuX() / 10, GravityRunner.CHAR_TILE_SIZE * GravityRunner.getInstance().getPpuX() / 10, new Vector2(1, -0.2f), 0);
        playerBody.friction = 0.0f;
        playerBody.density = 0.1f;
        playerBody.restitution = 0.1f;
        playerFeet.friction = 0.1f;
        playerFeet.density = 0.1f;
        playerFeet.restitution = 0.1f;
        playerLegs.friction = 0.0f;
        body.createFixture(playerBody).setUserData("playerBody");
        body.createFixture(playerFeet).setUserData("playerFeet");
        body.createFixture(playerLegs).setUserData("playerLegs");
        body.createFixture(playerHand).setUserData("playerHand");
        playerFeet.shape.dispose();
        playerLegs.shape.dispose();
        playerHand.shape.dispose();
        playerBody.shape.dispose();
    }

    public void update() {

    }

    public void act(float delta) {
        setPosition(body.getPosition().x - getWidth() / 13 * 7, body.getPosition().y - getHeight() / 2);

    }

    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY(), getWidth(), getHeight());
    }

    public Body getBody() {
        return body;
    }

}