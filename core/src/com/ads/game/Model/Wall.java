package com.ads.game.Model;

import com.ads.game.GravityRunner;
import com.ads.game.View.ImageActor;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import java.util.HashMap;

import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.StaticBody;

/**
 * Created by Aksi on 29.03.2016.
 */
public class Wall extends ImageActor {
    HashMap<String, TextureRegion> textureRegions;
    TextureRegion img;
    Body body;

    public Wall(TextureRegion img, float x, float y, World world, String textureNum) {
        this.img = img;
        setSize(GravityRunner.OUTPUT_TILE_SIZE * GravityRunner.getInstance().getPpuX(), GravityRunner.OUTPUT_TILE_SIZE * GravityRunner.getInstance().getPpuY());
        BodyDef bodyDef = new BodyDef();
        bodyDef.fixedRotation = false;

        bodyDef.position.x = x + getWidth() / 2;
        bodyDef.position.y = y + getHeight() / 2;
        bodyDef.type = StaticBody;

        body = world.createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = new PolygonShape();
        fixtureDef.friction = 0.8f;
        fixtureDef.density = 1f;
        fixtureDef.restitution = 0f;
        //Правая нижняя треугольная текстура
        if (textureNum == "03") {
            ((PolygonShape) fixtureDef.shape).set(new Vector2[]{
                    new Vector2(1.5f - getWidth(), 1.5f - getHeight()),
                    new Vector2(4.5f - getWidth(), 1.5f - getHeight()),
                    new Vector2(4.5f - getWidth(), 4.5f - getHeight())
            });
            body.createFixture(fixtureDef).setUserData("wall");
        }
        //Левая нижняя треугольная текстура
        if (textureNum == "04") {
            ((PolygonShape) fixtureDef.shape).set(new Vector2[]{
                    new Vector2(4.5f - getWidth(), 1.5f - getHeight()),
                    new Vector2(1.5f - getWidth(), 1.5f - getHeight()),
                    new Vector2(1.5f - getWidth(), 4.5f - getHeight())
            });
            body.createFixture(fixtureDef).setUserData("wall");
        }
        //Левая верхняя треугольная текстура
        if (textureNum == "05") {
            ((PolygonShape) fixtureDef.shape).set(new Vector2[]{
                    new Vector2(4.5f - getWidth(), 4.5f - getHeight()),
                    new Vector2(1.5f - getWidth(), 4.5f - getHeight()),
                    new Vector2(1.5f - getWidth(), 1.5f - getHeight())
            });
            body.createFixture(fixtureDef).setUserData("wall");
        }
        //Правая верхняя треугольная текстура
        if (textureNum == "06") {
            ((PolygonShape) fixtureDef.shape).set(new Vector2[]{
                    new Vector2(1.5f - getWidth(), 4.5f - getHeight()),
                    new Vector2(4.5f - getWidth(), 4.5f - getHeight()),
                    new Vector2(4.5f - getWidth(), 1.5f - getHeight())
            });
            body.createFixture(fixtureDef).setUserData("wall");
        }
        //Выключатель
        if (textureNum == "12") {
            ((PolygonShape) fixtureDef.shape).setAsBox(getWidth() / 2, getHeight() / 2);
            fixtureDef.isSensor = true;
            body.createFixture(fixtureDef).setUserData("switch");
        }
        //Стена
        if (textureNum == "01"||textureNum == "02") {
            ((PolygonShape) fixtureDef.shape).setAsBox(getWidth() / 2, getHeight() / 2);
            body.createFixture(fixtureDef).setUserData("wall");
        }
        //Платформа
        if (textureNum == "14") {
            ((PolygonShape) fixtureDef.shape).setAsBox(getWidth() / 2, getHeight() / 2);
            body.createFixture(fixtureDef).setUserData("jumpplatform");
        }
        if (textureNum == "d0") {
            ((PolygonShape) fixtureDef.shape).setAsBox(getWidth() / 2, getHeight() / 2);
            fixtureDef.isSensor = true;
            body.createFixture(fixtureDef).setUserData("dorexit");
        }
        fixtureDef.shape.dispose();
    }

    public void act(float delta) {
        setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - getHeight() / 2);

    }

    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY(), getWidth(), getHeight());
    }
}
