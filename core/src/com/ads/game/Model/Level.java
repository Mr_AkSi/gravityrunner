package com.ads.game.Model;

import com.ads.game.Controller.MyContactListener;
import com.ads.game.Controller.PlayerController;
import com.ads.game.Controller.State;
import com.ads.game.GravityRunner;
import com.ads.game.Screens.GameScreen;
import com.ads.game.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

import static com.ads.game.GravityRunner.getInstance;

/**
 * Created by Aksi on 25.01.2016.
 */
public class Level extends Stage {
    String[][] wall;
    World physicsWorld;
    Player player;
    PhysicsBox box1;
    Wall bridge;
    Vector2 Gravitaion = new Vector2(0, GravityRunner.G);
    public Level(ScreenViewport screenViewport, SpriteBatch batch, OrthographicCamera cam, String levelName) throws FileNotFoundException {
        super(screenViewport, batch);
        physicsWorld = new World(Gravitaion, false);
        physicsWorld.setContactListener(new MyContactListener(physicsWorld));
        cam.zoom = 0.03f;
        addPlayer(getInstance().textureRegions.get("playerStay"), GravityRunner.startX, GravityRunner.startY, physicsWorld);
        cam.position.set(player.getX(),player.getY(), 0);
        wall = fieldFromFile(Gdx.files.internal(levelName).path());
        for (int i = 0; i < wall.length; i++) {
            for (int j = 0; j < wall[i].length; j++)
                System.out.print(wall[i][j] + " ");
            System.out.println();
        }
        for (int i = 0; i < wall.length; i++) {
            ImageActor temp;
            Wall wallTmp;
            for (int j = 0; j < wall[i].length; j++) {
                if (wall[i][j].equals("00")) {
                    temp = new ImageActor(getInstance().textureRegions.get("white"), j * GravityRunner.OUTPUT_TILE_SIZE, i * GravityRunner.OUTPUT_TILE_SIZE, GravityRunner.OUTPUT_TILE_SIZE, GravityRunner.OUTPUT_TILE_SIZE);
                    addActor(temp);
                }
                if (wall[i][j].equals("01")) {
                    wallTmp = new Wall(getInstance().textureRegions.get("black1"), j * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuX(), i * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuY(), physicsWorld, "01");
                    addActor(wallTmp);
                }
                if (wall[i][j].equals("02")) {
                    wallTmp = new Wall(getInstance().textureRegions.get("black2"), j * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuX(), i * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuY(), physicsWorld, "02");
                    addActor(wallTmp);
                }
                if (wall[i][j].equals("03")) {
                    wallTmp = new Wall(getInstance().textureRegions.get("blackcornerrd"), j * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuX(), i * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuY(), physicsWorld, "03");
                    addActor(wallTmp);
                }
                if (wall[i][j].equals("04")) {
                    wallTmp = new Wall(getInstance().textureRegions.get("blackcornerld"), j * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuX(), i * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuY(), physicsWorld, "04");
                    addActor(wallTmp);
                }
                if (wall[i][j].equals("05")) {
                    wallTmp = new Wall(getInstance().textureRegions.get("blackcornerlu"), j * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuX(), i * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuY(), physicsWorld, "05");
                    addActor(wallTmp);
                }
                if (wall[i][j].equals("06")) {
                    wallTmp = new Wall(getInstance().textureRegions.get("blackcornerru"), j * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuX(), i * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuY(), physicsWorld, "06");
                    addActor(wallTmp);
                }
                if (wall[i][j].equals("d1")) {
                    temp = new ImageActor(getInstance().textureRegions.get("dortop"), j * GravityRunner.OUTPUT_TILE_SIZE, i * GravityRunner.OUTPUT_TILE_SIZE, GravityRunner.OUTPUT_TILE_SIZE, GravityRunner.OUTPUT_TILE_SIZE);
                    addActor(temp);
                }
                if (wall[i][j].equals("d2")) {
                    temp = new ImageActor(getInstance().textureRegions.get("dorbot"), j * GravityRunner.OUTPUT_TILE_SIZE, i * GravityRunner.OUTPUT_TILE_SIZE, GravityRunner.OUTPUT_TILE_SIZE, GravityRunner.OUTPUT_TILE_SIZE);
                    addActor(temp);
                }
                if (wall[i][j].equals("w1")) {
                    temp = new ImageActor(getInstance().textureRegions.get("windowopen"), j * GravityRunner.OUTPUT_TILE_SIZE, i * GravityRunner.OUTPUT_TILE_SIZE, GravityRunner.OUTPUT_TILE_SIZE, GravityRunner.OUTPUT_TILE_SIZE);
                    addActor(temp);
                }
                if (wall[i][j].equals("w2")) {
                    temp = new ImageActor(getInstance().textureRegions.get("windowclosed"), j * GravityRunner.OUTPUT_TILE_SIZE, i * GravityRunner.OUTPUT_TILE_SIZE, GravityRunner.OUTPUT_TILE_SIZE, GravityRunner.OUTPUT_TILE_SIZE);
                    addActor(temp);
                }
                if (wall[i][j].equals("w3")) {
                    temp = new ImageActor(getInstance().textureRegions.get("windowopen"), j * GravityRunner.OUTPUT_TILE_SIZE, i * GravityRunner.OUTPUT_TILE_SIZE, GravityRunner.OUTPUT_TILE_SIZE*3, GravityRunner.OUTPUT_TILE_SIZE*3);
                    addActor(temp);
                }
                if (wall[i][j].equals("11")) {
                    temp = new ImageActor(getInstance().textureRegions.get("vent"), j * GravityRunner.OUTPUT_TILE_SIZE, i * GravityRunner.OUTPUT_TILE_SIZE, GravityRunner.OUTPUT_TILE_SIZE, GravityRunner.OUTPUT_TILE_SIZE);
                    addActor(temp);
                }
                if (wall[i][j].equals("12")) {
                    wallTmp = new Wall(getInstance().textureRegions.get("switchoff"), j * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuX(), i * GravityRunner.OUTPUT_TILE_SIZE* getInstance().getPpuY(), physicsWorld, "12");
                    addActor(wallTmp);
                }
                if (wall[i][j].equals("13")) {
                    temp = new ImageActor(getInstance().textureRegions.get("switchon"), j * GravityRunner.OUTPUT_TILE_SIZE, i * GravityRunner.OUTPUT_TILE_SIZE, GravityRunner.OUTPUT_TILE_SIZE, GravityRunner.OUTPUT_TILE_SIZE);
                    addActor(temp);
                }
                if (wall[i][j].equals("14")) {
                    wallTmp = new Wall(getInstance().textureRegions.get("jumpplatform"), j * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuX(), i * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuY(), physicsWorld, "14");
                    addActor(wallTmp);
                }
                if (wall[i][j].equals("d0")) {
                    wallTmp = new Wall(getInstance().textureRegions.get("dorexit"), j * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuX(), i * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuY(), physicsWorld, "d0");
                    addActor(wallTmp);
                }
            }
        }
        addActor(player);

    }
    private int countRows(String fileName) throws FileNotFoundException {
        Scanner sc = new Scanner(Gdx.files.internal(fileName).read());
        int result = 0;
        while (sc.hasNext()) {
            sc.nextLine();
            result++;
        }
        sc.close();
        return result;
    }
    private int countColumns(String fileName) throws FileNotFoundException {
        Scanner sc = new Scanner(Gdx.files.internal(fileName).read());
        int result = 0;
        String tmp;
        while (sc.hasNext()) {
            tmp = sc.nextLine();
            result = Math.max(tmp.length(), result);
        }
        sc.close();
        return result / 2;
    }
    private String[][] fieldFromFile(String fileName) throws FileNotFoundException {
        int rows = countRows(Gdx.files.internal(fileName).path());
        int columns = countColumns(Gdx.files.internal(fileName).path());
        String[][] wall = new String[rows][columns];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++)
                wall[i][j] = "00";
        Scanner sc = new Scanner(Gdx.files.internal(fileName).read());
        String tmp;
        for (int i = 0; i < rows; i++) {
            tmp = sc.nextLine();
            for (int j = 0; j < columns; j++) {
                char[] chars = {tmp.charAt(2 * j), tmp.charAt(2 * j + 1)};
                wall[i][j] = new String(chars);
            }
        }
        sc.close();
        String[] temprow;
        for (int i = 0; i < wall.length / 2; i++) {
            temprow = wall[i];
            wall[i] = wall[wall.length - 1 - i];
            wall[wall.length - 1 - i] = temprow;
        }
        return wall;
    }
    public void act(float delta, OrthographicCamera cam) {
        super.act();
        if(GravityRunner.gamePaused == State.NO) {
            physicsWorld.step(1/60f, 6, 2);
            cam.translate((player.getX() - cam.position.x) / 30, (player.getY() - cam.position.y) / 20 + 0.2f);
        }
    }
    public Player getPlayer() {
        return player;
    }    public World getPhysicsWorld() {
        return physicsWorld;
    }
    public void addPlayer(TextureRegion playerT, float x, float y, World physicsWorld) {
        player = new Player(playerT, x, y, physicsWorld);
    }
    public void addWall(TextureRegion img, float x, float y, World world, String textureNum){
        bridge = new Wall(img, x * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuX(), y * GravityRunner.OUTPUT_TILE_SIZE * getInstance().getPpuY(), world, textureNum);
        addActor(bridge);
    }
    public void restartLvl() throws FileNotFoundException {
        getInstance().gameScreen.dispose();
        getInstance().showMenu();
        getInstance().gameScreen = new GameScreen(getInstance().batch);
        getInstance().showGame();
    }
}

