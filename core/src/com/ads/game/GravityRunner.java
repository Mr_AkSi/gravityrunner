package com.ads.game;

import com.ads.game.Controller.BarState;
import com.ads.game.Controller.MovementControlStyle;
import com.ads.game.Controller.Planet;
import com.ads.game.Controller.State;
import com.ads.game.Controller.ToggleState;
import com.ads.game.Screens.GameScreen;
import com.ads.game.Screens.DifficultyScreen;
import com.ads.game.Screens.MainMenuScreen;
import com.ads.game.Screens.SettingsScreen;
import com.ads.game.Screens.StatsScreen;
import com.ads.game.Screens.WinScreen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.io.FileNotFoundException;
import java.util.HashMap;

import javafx.application.Application;

/**
 * Created by Aksi on 25.01.2016.
 */
public class GravityRunner extends com.badlogic.gdx.Game {
	//Элементы
	private float ppuX, ppuY;
	public SpriteBatch batch;
	public HashMap<String, TextureRegion> textureRegions;
	private ShapeRenderer shape;
	private BitmapFont font;
	private MainMenuScreen menuScreen;
	public GameScreen gameScreen;
	private DifficultyScreen difficultyScreen;
	private SettingsScreen settingsScreen;
	public StatsScreen statsScreen;
	public WinScreen winScreen;
	private ToggleState soundState;
	private ToggleState musicState;
	private ToggleState brightnessState;
	private BarState soundBarState;
	private BarState musicBarState;
	private BarState brightnessBarState;
	public static Planet planet = Planet.MOON;
	//Управление
	public static final MovementControlStyle movementControlStyle = MovementControlStyle.BUTTONS;
	//Размеры
	public static final float WORLD_WIDTH = 16f;
	public static final float WORLD_HEIGHT = 9f;
	public static final int TILE_SIZE = 512;
	public static final float CHAR_TILE_SIZE = 0.08f;
	public static final float OUTPUT_TILE_SIZE = 0.05f;
	//Дебаг
	public static final boolean DEBUG = false;
	public static final boolean PHYSICS_DEBUG = false;
	//Стейты
	public static State inAir = State.NO;
	public static State nearSwitch = State.NO;
	public static State gamePaused = State.YES;
	public static State jumpboost = State.NO;
	public static float G = 0;
	//Положение игрока на карте
	public static float startX = 23f;
	public static float startY = 80f;
	//Время
	public static int TotalTime = 0;
	public static int FiveSecsOfAchiv = 0;
	public static int timerLVL1 = 0;
	public static int timerLVL2 = 0;
	public static int timerLVL3 = 0;
	public static int timerLVL4 = 0;
	public static int timerLVL5 = 0;
	public static int timerLVL6 = 0;
	public static int timerLVL7 = 0;
	public static int timerLVL8 = 0;
	//Статистика
	public static int jumps = 0;
	//Ачивки
	public static boolean FiftyJumps = false;
	//Пройденные уровни
	public static boolean Level1Completed = false;
	public static boolean Level2Completed = false;
	public static boolean Level3Completed = false;
	public static boolean Level4Completed = false;
	public static boolean Level5Completed = false;
	public static boolean Level6Completed = false;
	public static boolean Level7Completed = false;
	public static boolean Level8Completed = false;

	public String level1;
	//Инстансы
	private static GravityRunner instance = new GravityRunner();

	public static GravityRunner getInstance() {
		return instance;
	}

	private GravityRunner() {
	}

	@Override
	public void create() {

		ppuX = Gdx.graphics.getWidth() / WORLD_WIDTH;
		ppuY = Gdx.graphics.getHeight() / WORLD_HEIGHT;
		setSoundState(ToggleState.ON);
		setMusicState(ToggleState.ON);
		setBrightnessState(ToggleState.ON);
		setSoundBarState(BarState.BAR7);
		setMusicBarState(BarState.BAR7);
		setBrightnessBarState(BarState.BAR7);
		loadGraphics();
		batch = new SpriteBatch();
		shape = new ShapeRenderer();
		font = new BitmapFont();
		menuScreen = new MainMenuScreen(batch);
		difficultyScreen = new DifficultyScreen(batch);
		settingsScreen = new SettingsScreen(batch);
		statsScreen = new StatsScreen(batch);
		winScreen = new WinScreen(batch);
		setScreen(menuScreen);
	}

	private void loadGraphics() {
		textureRegions = new HashMap<String, TextureRegion>();
		Texture atlas = new Texture(pathLoader("Walls/Walls.png"));
		textureRegions.put("white", new TextureRegion(atlas, 0, 0, TILE_SIZE, TILE_SIZE));
		textureRegions.put("black1", new TextureRegion(atlas, TILE_SIZE, 0, TILE_SIZE, TILE_SIZE));
		textureRegions.put("black2", new TextureRegion(atlas, TILE_SIZE * 2, 0, TILE_SIZE, TILE_SIZE));
		textureRegions.put("blackcornerrd", new TextureRegion(atlas, TILE_SIZE * 3, 0, TILE_SIZE, TILE_SIZE));
		textureRegions.put("blackcornerld", new TextureRegion(atlas, TILE_SIZE * 4, 0, TILE_SIZE, TILE_SIZE));
		textureRegions.put("blackcornerlu", new TextureRegion(atlas, TILE_SIZE * 5, 0, TILE_SIZE, TILE_SIZE));
		textureRegions.put("blackcornerru", new TextureRegion(atlas, TILE_SIZE * 6, 0, TILE_SIZE, TILE_SIZE));
		textureRegions.put("dortop", new TextureRegion(atlas, TILE_SIZE * 7, 0, TILE_SIZE, TILE_SIZE));
		textureRegions.put("dorbot", new TextureRegion(atlas, TILE_SIZE * 8, 0, TILE_SIZE, TILE_SIZE));
		textureRegions.put("windowopen", new TextureRegion(atlas, TILE_SIZE * 9, 0, TILE_SIZE, TILE_SIZE));
		textureRegions.put("windowclosed", new TextureRegion(atlas, TILE_SIZE * 10, 0, TILE_SIZE, TILE_SIZE));
		textureRegions.put("vent", new TextureRegion(atlas, TILE_SIZE * 11, 0, TILE_SIZE, TILE_SIZE));
		textureRegions.put("switchoff", new TextureRegion(atlas, TILE_SIZE * 12, 0, TILE_SIZE, TILE_SIZE));
		textureRegions.put("switchon", new TextureRegion(atlas, TILE_SIZE * 13, 0, TILE_SIZE, TILE_SIZE));
		textureRegions.put("jumpplatform", new TextureRegion(atlas, TILE_SIZE * 14, 0, TILE_SIZE, TILE_SIZE));
		textureRegions.put("dorexit", new TextureRegion(atlas, TILE_SIZE * 15, 0, TILE_SIZE, TILE_SIZE));
		textureRegions.put("playerStay", new TextureRegion(new Texture(pathLoader("Animations/Waiting/Waiting0001.png"))));
		textureRegions.put("playerJump", new TextureRegion(new Texture(pathLoader("Animations/Jump.png"))));
		textureRegions.put("pauseBG", new TextureRegion(new Texture(pathLoader("PauseBG.png"))));
		textureRegions.put("empty", new TextureRegion(new Texture(pathLoader("empty.png"))));
		textureRegions.put("PlayBtn", new TextureRegion(new Texture(pathLoader("Buttons/Play.png"))));
		textureRegions.put("MenuBtn", new TextureRegion(new Texture(pathLoader("Buttons/Back.png"))));
		textureRegions.put("ResetBtn", new TextureRegion(new Texture(pathLoader("Buttons/Reset.png"))));
		textureRegions.put("MusicOnBtn", new TextureRegion(new Texture(pathLoader("Buttons/Music.png"))));
		textureRegions.put("MusicOffBtn", new TextureRegion(new Texture(pathLoader("Buttons/MusicOff.png"))));
		textureRegions.put("SoundOnBtn", new TextureRegion(new Texture(pathLoader("Buttons/Volume.png"))));
		textureRegions.put("SoundOffBtn", new TextureRegion(new Texture(pathLoader("Buttons/VolumeOff.png"))));
		textureRegions.put("BrightnessOnBtn", new TextureRegion(new Texture(pathLoader("Buttons/Brightness.png"))));
		textureRegions.put("BrightnessOffBtn", new TextureRegion(new Texture(pathLoader("Buttons/BrightnessOff.png"))));
		textureRegions.put("Achivment", new TextureRegion(new Texture(pathLoader("Buttons/LongButton.png"))));
		textureRegions.put("moon", new TextureRegion(new Texture(pathLoader("1.jpg"))));
		textureRegions.put("upiter", new TextureRegion(new Texture(pathLoader("2.jpg"))));
		textureRegions.put("earth", new TextureRegion(new Texture(pathLoader("3.jpg"))));
		textureRegions.put("bar0", new TextureRegion(new Texture(pathLoader("Bar/Bar0.png"))));
		textureRegions.put("bar1", new TextureRegion(new Texture(pathLoader("Bar/Bar1.png"))));
		textureRegions.put("bar2", new TextureRegion(new Texture(pathLoader("Bar/Bar2.png"))));
		textureRegions.put("bar3", new TextureRegion(new Texture(pathLoader("Bar/Bar3.png"))));
		textureRegions.put("bar4", new TextureRegion(new Texture(pathLoader("Bar/Bar4.png"))));
		textureRegions.put("bar5", new TextureRegion(new Texture(pathLoader("Bar/Bar5.png"))));
		textureRegions.put("bar6", new TextureRegion(new Texture(pathLoader("Bar/Bar6.png"))));
		textureRegions.put("bar7", new TextureRegion(new Texture(pathLoader("Bar/Bar7.png"))));
		textureRegions.put("background", new TextureRegion(new Texture(pathLoader("background.png"))));
		textureRegions.put("easy", new TextureRegion(new Texture(pathLoader("Buttons/Easy.png"))));
		textureRegions.put("medium", new TextureRegion(new Texture(pathLoader("Buttons/Medium.png"))));
		textureRegions.put("hard", new TextureRegion(new Texture(pathLoader("Buttons/Hard.png"))));
		textureRegions.put("logo", new TextureRegion(new Texture(pathLoader("Logo.png"))));
		textureRegions.put("stats", new TextureRegion(new Texture(pathLoader("Buttons/Stats.png"))));
		textureRegions.put("settings", new TextureRegion(new Texture(pathLoader("Buttons/Settings.png"))));
		textureRegions.put("timer", new TextureRegion(new Texture(pathLoader("Timer.png"))));
		textureRegions.put("top3", new TextureRegion(new Texture(pathLoader("Top3.png"))));
		level1 = pathLoader("Levels/level1.txt")+"";
	}
	public FileHandle pathLoader(String path) {
		return Gdx.files.internal(String.format("%s%s", Gdx.app.getType().equals(com.badlogic.gdx.Application.ApplicationType.Desktop) ? "android/assets/" : "", path));
	}

	public void showGame() {
		setScreen(gameScreen);
		GravityRunner.gamePaused = State.NO;
	}

	public void showLevels() {
		setScreen(difficultyScreen);
	}

	public void showStats() {
		statsScreen.setTimer();
		setScreen(statsScreen);
	}

	public void showMenu() {
		setScreen(menuScreen);
	}

	public void showSettings() {
		setScreen(settingsScreen);
	}

	public void showWinScreen() {
		setScreen(winScreen);
	}

	public void render() {
		super.render();
	}

	public float getPpuX() {
		return ppuX;
	}

	public float getPpuY() {
		return ppuY;
	}

	public ShapeRenderer getShape() {
		return shape;
	}

	public ToggleState getSoundState() {
		return soundState;
	}

	public void setSoundState(ToggleState soundState) {
		this.soundState = soundState;
	}

	public void setMusicState(ToggleState musicState) {
		this.musicState = musicState;
	}

	public void setBrightnessState(ToggleState brightnessState) {
		this.brightnessState = brightnessState;
	}

	public BarState getSoundBarState() {
		return soundBarState;
	}

	public void setSoundBarState(BarState soundBarState) {
		this.soundBarState = soundBarState;
	}

	public BarState getMusicBarState() {
		return musicBarState;
	}

	public void setMusicBarState(BarState musicBarState) {
		this.musicBarState = musicBarState;
	}

	public BarState getBrightnessBarState() {
		return brightnessBarState;
	}

	public void setBrightnessBarState(BarState brightnessBarState) {
		this.brightnessBarState = brightnessBarState;
	}

	public ToggleState getMusicState() {
		return musicState;
	}

	public ToggleState getBrightnessState() {
		return brightnessState;
	}
}
