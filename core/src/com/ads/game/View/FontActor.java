package com.ads.game.View;

import com.ads.game.GravityRunner;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Aksi on 28.03.2016.
 */
public class FontActor extends Actor {
    String string;
    BitmapFont font;

    public FontActor(String s, float x, float y, BitmapFont font) {
        this.font = font;
        this.string = s;
        this.setPosition(x * GravityRunner.getInstance().getPpuX(), y* GravityRunner.getInstance().getPpuY());
    }

    public void draw(Batch batch, float parentAlpha) {
        font.draw(batch, string, getX(), getY());
    }
    public void setString(String s){
        this.string = s;
    }
}
