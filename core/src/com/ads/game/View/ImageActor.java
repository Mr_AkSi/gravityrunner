package com.ads.game.View;

import com.ads.game.GravityRunner;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Aksi on 25.01.2016.
 */
public class ImageActor extends Actor {
    public TextureRegion img;
    ShapeRenderer shapeRenderer;

    public ImageActor(TextureRegion img, float x, float y, float width, float height) {
        this.img = img;
        setPosition(x * GravityRunner.getInstance().getPpuX(), y * GravityRunner.getInstance().getPpuY());
        setSize(width * GravityRunner.getInstance().getPpuX(), height * GravityRunner.getInstance().getPpuY());
        shapeRenderer = GravityRunner.getInstance().getShape();
    }

    public ImageActor(TextureRegion img, float x, float y, float width, float height, String name) {
        this.img = img;
        setName(name);
        setPosition(x * GravityRunner.getInstance().getPpuX(), y * GravityRunner.getInstance().getPpuY());
        setSize(width * GravityRunner.getInstance().getPpuX(), height * GravityRunner.getInstance().getPpuY());
        shapeRenderer = GravityRunner.getInstance().getShape();
    }

    public ImageActor(Texture img, float x, float y, float width, float height) {
        this(new TextureRegion(img), x, y, width, height);
    }
    public ImageActor(){

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY(), getWidth(), getHeight());
        if (GravityRunner.DEBUG) {
            batch.end();
            if (shapeRenderer.getProjectionMatrix() == null) {
                shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
            }
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(Color.RED);
            shapeRenderer.rect(getX() - 1, getY() - 1, getWidth() + 2, getHeight() + 2);
            shapeRenderer.end();
            batch.begin();
        }
    }
}
