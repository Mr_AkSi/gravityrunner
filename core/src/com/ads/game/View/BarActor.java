package com.ads.game.View;

import com.ads.game.Controller.BarState;
import com.ads.game.GravityRunner;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Aksi on 22.03.2016.
 */
public class BarActor extends Actor {
        private BarState barstate;

    public BarActor(float x, float y, float w, float h) {
        setPosition(x * GravityRunner.getInstance().getPpuX(), y * GravityRunner.getInstance().getPpuY());
        setSize(w * GravityRunner.getInstance().getPpuX(), h * GravityRunner.getInstance().getPpuY());
    }

    public BarActor() {
    }

    public void draw(Batch batch, float parentAlpha) {
        if (barstate == BarState.BAR0)
            batch.draw(GravityRunner.getInstance().textureRegions.get("bar0"), getX(), getY(), getWidth(), getHeight());
        if (barstate == BarState.BAR1)
            batch.draw(GravityRunner.getInstance().textureRegions.get("bar1"), getX(), getY(), getWidth(), getHeight());
        if (barstate == BarState.BAR2)
            batch.draw(GravityRunner.getInstance().textureRegions.get("bar2"), getX(), getY(), getWidth(), getHeight());
        if (barstate == BarState.BAR3)
            batch.draw(GravityRunner.getInstance().textureRegions.get("bar3"), getX(), getY(), getWidth(), getHeight());
        if (barstate == BarState.BAR4)
            batch.draw(GravityRunner.getInstance().textureRegions.get("bar4"), getX(), getY(), getWidth(), getHeight());
        if (barstate == BarState.BAR5)
            batch.draw(GravityRunner.getInstance().textureRegions.get("bar5"), getX(), getY(), getWidth(), getHeight());
        if (barstate == BarState.BAR6)
            batch.draw(GravityRunner.getInstance().textureRegions.get("bar6"), getX(), getY(), getWidth(), getHeight());
        if (barstate == BarState.BAR7)
            batch.draw(GravityRunner.getInstance().textureRegions.get("bar7"), getX(), getY(), getWidth(), getHeight());

    }

    public void setState(BarState state) {
        this.barstate = state;
    }

    public BarState getState() {
        return barstate;
    }

    public void addListener(InputProcessor inputProcessor) {

    }

}