package com.ads.game.View;

import com.ads.game.Controller.BarState;
import com.ads.game.Controller.ToggleState;
import com.ads.game.GravityRunner;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Aksi on 25.01.2016.
 */
public class ToggleActor extends Actor {
    TextureRegion imgOn;
    TextureRegion imgOff;
    private ToggleState state;

    public ToggleActor(TextureRegion imgOn, TextureRegion imgOff, float x, float y, float w, float h) {
        setPosition(x * GravityRunner.getInstance().getPpuX(), y * GravityRunner.getInstance().getPpuY());
        setSize(w * GravityRunner.getInstance().getPpuX(), h * GravityRunner.getInstance().getPpuY());
        this.imgOn = imgOn;
        this.imgOff = imgOff;
    }

    public void draw(Batch batch, float parentAlpha) {
        if (state == ToggleState.ON)
            batch.draw(imgOn, getX(), getY(), getWidth(), getHeight());
        else
            batch.draw(imgOff, getX(), getY(), getWidth(), getHeight());
    }

    public void setState(ToggleState state) {
        this.state = state;
    }

    public ToggleState getState() {
        return state;
    }

    public void toggle() {
        if (state == ToggleState.OFF)
            state = ToggleState.ON;
        else
            state = ToggleState.OFF;
        BarActor barActor = new BarActor();
        barActor.setState(BarState.BAR0);
    }
}
