package com.ads.game.android;

import android.os.Bundle;

import com.ads.game.GravityRunner;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		System.setProperty("user.name","EnglishWords");
				initialize(GravityRunner.getInstance(), config);
	}
}
