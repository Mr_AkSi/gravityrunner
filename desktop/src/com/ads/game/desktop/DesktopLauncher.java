package com.ads.game.desktop;

import com.ads.game.GravityRunner;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		System.setProperty("user.name","EnglishWords");
		config.width = 960;
		config.height = 540;
		new LwjglApplication(GravityRunner.getInstance(), config);
	}
}
